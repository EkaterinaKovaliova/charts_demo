import styled from 'styled-components';

export const MainChartsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  flex: 1;
  flex-wrap: wrap;
  padding: 20px;
`;
