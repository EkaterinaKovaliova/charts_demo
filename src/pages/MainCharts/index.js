import React from 'react';

import BarChart from '../../modules/Charts/components/BarChart';
import CombineChart from '../../modules/Charts/components/CombineChart';
import LineChart from '../../modules/Charts/components/LineChart';
import PieChart from '../../modules/Charts/components/PieChart';

import { MainChartsWrapper } from './MainChartsStyles';

function MainCharts() {
  return (
    <MainChartsWrapper>
      <LineChart />
      <BarChart />
      <CombineChart />
      <PieChart />
    </MainChartsWrapper>
  );
}

export default MainCharts;
