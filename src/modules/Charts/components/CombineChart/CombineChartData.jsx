import React, { Fragment } from 'react';
import { line, curveCatmullRom } from 'd3-shape';

export const CombineBarData = [
  { x: '0', v: 3.3, v1: 3.5, v2: 2.1, v3: 1.2 },
  { x: '1', v: 4.5, v1: 4.1, v2: 2.9, v3: 1.3 },
  { x: '2', v: 3.8, v1: 3.7, v2: 2.5, v3: 1.1 },
  { x: '3', v: 4.1, v1: 4.1, v2: 2.0, v3: 1.3 },
  { x: '4', v: 6.4, v1: 6.0, v2: 4, v3: 2.6 },
  { x: '5', v: 4.7, v1: 4.9, v2: 3, v3: 2.7 },
  { x: '6', v: 7.9, v1: 7.9, v2: 4, v3: 2.3 },
  { x: '7', v: 5.2, v1: 5.3, v2: 4.1, v3: 1.8 },
];

const colors = {
  line1Color: '#1fb30b',
  line2Color: '#2c290b',
  line3Color: '#0000ae',
};

export const Line1 = ({ bars, xScale, yScale }) => {
  const lineGenerator = line()
    .x((bar) => (bar.data.index === 0 ? 0 : xScale(bar.data.index) + bar.width))
    .y((bar) => yScale(bar.data.data.v1))
    .curve(curveCatmullRom.alpha(0.5));

  return (
    <Fragment>
      <path
        d={lineGenerator(bars)}
        fill='none'
        stroke={colors.line1Color}
        style={{ pointerEvents: 'none' }}
      />
    </Fragment>
  );
};

export const Line2 = ({ bars, xScale, yScale }) => {
  const lineGenerator = line()
    .x((bar) => (bar.data.index === 0 ? 0 : xScale(bar.data.index) + bar.width))
    .y((bar) => yScale(bar.data.data.v2))
    .curve(curveCatmullRom.alpha(0.5));

  return (
    <Fragment>
      <path
        d={lineGenerator(bars)}
        fill='none'
        stroke={colors.line2Color}
        style={{ pointerEvents: 'none' }}
      />
    </Fragment>
  );
};

export const Line3 = ({ bars, xScale, yScale }) => {
  const lineGenerator = line()
    .x((bar) => (bar.data.index === 0 ? 0 : xScale(bar.data.index) + bar.width))
    .y((bar) => yScale(bar.data.data.v3))
    .curve(curveCatmullRom.alpha(0.5));

  return (
    <Fragment>
      <path
        d={lineGenerator(bars)}
        fill='none'
        stroke={colors.line3Color}
        style={{ pointerEvents: 'none' }}
      />
    </Fragment>
  );
};
