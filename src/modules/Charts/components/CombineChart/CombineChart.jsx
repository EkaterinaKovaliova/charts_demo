import React from 'react';
import { ResponsiveBar } from '@nivo/bar';

import {
  CombineBarData as data,
  Line1,
  Line2,
  Line3,
} from './CombineChartData.jsx';
import { CombineChartWrapper, CombineTitle } from './CombineChartStyles';

function CombineChart() {
  const baseLegend = {
    anchor: 'top-right',
    direction: 'row',
    justify: false,
    translateX: 0,
    itemWidth: 70,
    itemHeight: 25,
    itemTextColor: '#999',
    itemDirection: 'left-to-right',
    itemOpacity: 1,
    symbolShape: 'circle',
    effects: [
      {
        on: 'hover',
        style: {
          itemTextColor: '#000',
        },
      },
    ],
  };

  const legends = [
    {
      ...baseLegend,
      translateY: -20,
      translateX: 10,
      data: [
        {
          id: '1',
          label: 'Line 1',
          color: '#1fb30b',
        },
        {
          id: '2',
          label: 'Line 2',
          color: '#2c290b',
        },
        {
          id: '3',
          label: 'Line 3',
          color: '#0000ae',
        },
      ],
    },
  ];
  return (
    <CombineChartWrapper>
      <CombineTitle>Combine Chart</CombineTitle>
      <ResponsiveBar
        data={data}
        keys={['v']}
        indexBy='x'
        maxValue={10}
        colors={['#878787']}
        margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
        padding={0.03}
        valueScale={{ type: 'linear' }}
        indexScale={{ type: 'band', round: true }}
        valueFormat={{ format: '', enabled: false }}
        borderColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
        borderRadius={5}
        axisTop={null}
        axisRight={null}
        axisBottom={{
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legendPosition: 'middle',
          legendOffset: 32,
        }}
        axisLeft={{
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legendPosition: 'middle',
          legendOffset: -40,
        }}
        enableLabel={false}
        layers={[
          'grid',
          'axes',
          'bars',
          'A',
          Line1,
          Line2,
          Line3,
          'markers',
          'legends',
        ]}
        legends={legends}
      />
    </CombineChartWrapper>
  );
}

export default CombineChart;
