import styled from 'styled-components';

export const CombineChartWrapper = styled.div`
  width: 650px;
  height: 400px;
  margin: 30px;
`;

export const CombineTitle = styled.h3`
  text-align: center;
`;
