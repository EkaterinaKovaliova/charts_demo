import styled from 'styled-components';

export const PieChartWrapper = styled.div`
  width: 650px;
  height: 400px;
  margin: 30px;
`;

export const PieChartTitle = styled.h3`
  text-align: center;
`;
