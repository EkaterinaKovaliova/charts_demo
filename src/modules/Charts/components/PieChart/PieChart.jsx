import React from 'react';
import { ResponsivePie } from '@nivo/pie';

import { PieChartData, CenteredMetric } from './PieChartData';
import { PieChartWrapper, PieChartTitle } from './PieChartStyles';

function PieChart() {
  return (
    <>
      <PieChartWrapper>
        <PieChartTitle>Pie Chart</PieChartTitle>
        <ResponsivePie
          data={PieChartData}
          margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
          colors={PieChartData.map((d) => d.color)}
          innerRadius={0.92}
          padAngle={0}
          activeOuterRadiusOffset={8}
          cornerRadius={2}
          borderWidth={1}
          borderColor={{ from: 'color', modifiers: [['darker', 0.1]] }}
          enableArcLabels={false}
          enableArcLinkLabels={false}
          legends={[
            {
              anchor: 'bottom',
              direction: 'row',
              justify: false,
              translateX: 0,
              translateY: 56,
              itemsSpacing: 0,
              itemWidth: 200,
              itemHeight: 18,
              itemTextColor: '#000',
              itemDirection: 'left-to-right',
              itemOpacity: 1,
              symbolSize: 0,
            },
          ]}
          layers={['arcs', 'legends', CenteredMetric]}
        />
      </PieChartWrapper>
    </>
  );
}

export default PieChart;
