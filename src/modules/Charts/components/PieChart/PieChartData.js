export const PieChartData = [
  {
    id: 'solar',
    label: '106 kWh Used From Solar',
    value: 77,
    color: '#878787',
  },
  {
    id: 'grid',
    label: '30 kWh Used From Grid',
    value: 33,
    color: '#e0e0e0',
  },
];

export const CenteredMetric = ({ centerX, centerY }) => {
  return (
    <text
      x={centerX}
      y={centerY}
      textAnchor='middle'
      dominantBaseline='central'
      style={{
        width: '50px',
        fontSize: '24px',
        fontWeight: 600,
      }}
    >
      77 % Used From Solar
    </text>
  );
};
