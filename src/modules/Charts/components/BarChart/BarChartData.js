export const BarChartData = [
  {
    month: 'Yan',
    energy: 36,
  },
  {
    month: 'Feb',
    energy: 28,
  },
  {
    month: 'Mar',
    energy: 35,
  },
  {
    month: 'Apr',
    energy: 24,
  },
  {
    month: 'May',
    energy: 33,
  },
  {
    month: 'Jun',
    energy: 23,
  },
  {
    month: 'Jul',
    energy: 36,
  },
  {
    month: 'Aug',
    energy: 26,
  },
  {
    month: 'Sep',
    energy: 34,
  },
  {
    month: 'Okt',
    energy: 22,
  },
  {
    month: 'Now',
    energy: 30,
  },
  {
    month: 'Dec',
    energy: 20,
  },
];
