import styled from 'styled-components';

export const BarChartWrapper = styled.div`
  width: 650px;
  height: 400px;
  margin: 30px;
`;

export const BarTitle = styled.h3`
  text-align: center;
`;
