import React from 'react';
import { ResponsiveBar } from '@nivo/bar';

import { BarChartData } from './BarChartData';
import { BarChartWrapper, BarTitle } from './BarChartStyles';

function BarChart() {
  const baseLegend = {
    anchor: 'top-right',
    direction: 'row',
    justify: false,
    translateX: 0,
    itemWidth: 70,
    itemHeight: 25,
    itemDirection: 'left-to-right',
    itemOpacity: 1,
    itemTextColor: 'black',
  };

  const legends = [
    {
      ...baseLegend,
      translateY: -20,
      translateX: -60,
      symbolSize: 0,
      data: [
        {
          id: '1',
          label: 'Total, kWh: 33.458,291',
        },
      ],
    },
  ];

  return (
    <BarChartWrapper>
      <BarTitle>Bar Chart</BarTitle>
      <ResponsiveBar
        markers={[
          {
            axis: 'y',
            value: 34,
            lineStyle: { stroke: 'rgba(0, 0, 0, .35)', strokeWidth: 2 },
            legendOrientation: 'horizontal',
          },
          {
            axis: 'x',
            value: 0,
            lineStyle: { stroke: '#8b858554', strokeWidth: 1 },
            legendOrientation: 'vertical',
          },
        ]}
        data={BarChartData}
        keys={['energy']}
        maxValue={60}
        indexBy='month'
        margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
        padding={0.3}
        valueScale={{ type: 'linear' }}
        indexScale={{ type: 'band', round: true }}
        valueFormat={{ format: '', enabled: false }}
        colors={['#878787']}
        borderRadius={3}
        borderColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
        enableLabel={false}
        axisTop={null}
        axisRight={null}
        axisBottom={{
          tickSize: 0,
          tickPadding: 5,
          tickRotation: 0,
          legendPosition: 'middle',
          legendOffset: 32,
        }}
        axisLeft={{
          tickValues: 4,
          tickSize: 0,
          tickPadding: 5,
          tickRotation: 0,
          format: (v) => `${v} k`,
          legendPosition: 'middle',
          legendOffset: -40,
        }}
        labelSkipWidth={12}
        labelSkipHeight={12}
        labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
        legends={legends}
      />
    </BarChartWrapper>
  );
}

export default BarChart;
