import styled from 'styled-components';

export const LineChartWrapper = styled.div`
  width: 650px;
  height: 400px;
  margin: 30px;
`;

export const TitleWrapper = styled.h3`
  text-align: center;
`;
