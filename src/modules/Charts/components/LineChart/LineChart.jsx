import React from 'react';
import { ResponsiveLine } from '@nivo/line';

import { LineChartData } from './LineChartData';
import { LineChartWrapper, TitleWrapper } from './LineChartStyles';

function LineChart() {
  return (
    <>
      <LineChartWrapper>
        <TitleWrapper>Line Chart</TitleWrapper>
        <ResponsiveLine
          data={LineChartData}
          curve='linear'
          colors={LineChartData.map((d) => d.color)}
          maxValue={2400}
          lineWidth={1}
          colorBy='index'
          enablePoints={false}
          enableGridX={false}
          margin={{ top: 40, right: 110, bottom: 50, left: 60 }}
          xScale={{ type: 'point' }}
          yScale={{
            type: 'linear',
            min: '0',
            max: '2400',
            stacked: true,
            reverse: false,
          }}
          yFormat=' >-.0f'
          axisTop={null}
          axisRight={null}
          axisBottom={{
            tickValues: [
              0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240,
            ],
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            format: '.2f',
            legendOffset: 36,
            legendPosition: 'middle',
          }}
          axisLeft={{
            orient: 'left',
            tickValues: [200, 1200, 2200],
            tickSize: 0,
            tickPadding: 5,
            tickRotation: 0,
            legendOffset: -40,
            legendPosition: 'middle',
          }}
          pointSize={10}
          pointColor={{ theme: 'background' }}
          pointBorderWidth={2}
          pointBorderColor={{ from: 'serieColor' }}
          pointLabelYOffset={-12}
          useMesh={true}
          legends={[
            {
              anchor: 'top-right',
              direction: 'row',
              justify: false,
              translateX: 17,
              translateY: 0,
              itemsSpacing: 0,
              itemDirection: 'left-to-right',
              itemWidth: 80,
              itemHeight: 20,
              itemOpacity: 0.75,
              symbolSize: 12,
              symbolBorderColor: 'rgba(0, 0, 0, .5)',
              effects: [
                {
                  on: 'hover',
                  style: {
                    itemBackground: 'rgba(0, 0, 0, .03)',
                    itemOpacity: 1,
                  },
                },
              ],
            },
          ]}
        />
      </LineChartWrapper>
    </>
  );
}

export default LineChart;
